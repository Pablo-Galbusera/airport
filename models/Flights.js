var mongoose = require('mongoose');

var FlightsSchema = new mongoose.Schema({
  order: Number,
  name: String,
  place: String,
  begin: Number,
  status: String,
  delay: Number
});

FlightsSchema.methods.upvote = function(cb) {
  // this.upvotes += 1;
  // this.save(cb);
};

mongoose.model('Flight', FlightsSchema);

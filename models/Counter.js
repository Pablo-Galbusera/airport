var mongoose = require('mongoose');

var CounterSchema = new mongoose.Schema({
  title: String,
  upvotes: {type: Number, default: 0},
});

CounterSchema.methods.upvote = function(cb) {
  this.upvotes += 1;
  this.save(cb);
};

mongoose.model('Counter', CounterSchema);

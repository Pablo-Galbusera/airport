var app = angular.module('airport', ['ui.router']);

app.factory('posts', ['$http', '$interval', '$location', function($http, $interval,$location){
  var o = {
    posts: [],
    counters: []
  };

  // if ($location.hash()=='superinternet') {
  //
  //   $interval(loadPosts, 2000);
  //
  //   function loadPosts () {
  //     $http.get('/posts').success(function(data){
  //       angular.copy(data, o.posts);
  //     });
  //   }
  //
  //   o.posts.automaticCounter = 0;
  //
  //   $interval(saveAutomaticPosts, 10000);
  //
  //   function saveAutomaticPosts() {
  //     o.posts.automaticCounter++;
  //     var post = {};
  //     post.title = 'testone '+o.posts.automaticCounter;
  //     $http.post('/posts', post).success(function(data){
  //       o.posts.push(data);
  //     });
  //   }
  //
  // }

  $interval(updateInfos, 2000);

  function updateInfos() {
    $http.get('/posts').success(function(data){
      angular.copy(data, o.posts);
    });
    $http.get('/counter').success(function(data){
      angular.copy(data, o.counters);
    });
  };

  o.getAll = function() {
    return $http.get('/posts').success(function(data){
      angular.copy(data, o.posts);
      $http.get('/counter').success(function(data){
        angular.copy(data, o.counters);
      });
    });
  };

  o.getCounters = function () {
    return $http.get('/counter').success(function (data) {
      // console.log(data);
      angular.copy(data, o.counters);
    });
  }

  o.create = function(post) {
    return $http.post('/posts', post).success(function(data){
      o.posts.push(data);
    });
  };

  o.upvote = function(post) {
    return $http.put('/posts/' + post._id + '/upvote')
      .success(function(data){
        post.upvotes += 1;
      });
  };

  o.upvoteCounter = function(counter) {
    console.log(counter);
    return $http.put('/counters/' + counter._id + '/upvote')
      .success(function(data){
        counter.upvotes += 1;
      });
  };

  o.update = function (post) {
    console.log(post);
    return $http.post('/posts/' + post._id + '/update', post).success(function(data){
      if (data.test == 'ok') {
        alert('Ho salvato zio');
      }
      // o.posts.push(data);
    });
  }

  o.get = function(id) {
    return $http.get('/posts/' + id).then(function(res){
      return res.data;
    });
  };

  o.addComment = function(id, comment) {
    return $http.post('/posts/' + id + '/comments', comment);
  };

  o.upvoteComment = function(post, comment) {
    return $http.put('/posts/' + post._id + '/comments/'+ comment._id + '/upvote')
      .success(function(data){
        comment.upvotes += 1;
      });
  };

  return o;
}]);

app.config([
'$stateProvider',
'$urlRouterProvider',
'$locationProvider',
function($stateProvider, $urlRouterProvider, $locationProvider) {

  $stateProvider
    .state('superinternetloveyou', {
      url: '/superinternetloveyou',
      templateUrl: '/home.html',
      controller: 'MainCtrl',
      resolve: {
        postPromise: ['posts', function(posts){
          return posts.getAll();
        }]
      }
    })
    .state('posts', {
      url: '/posts/{id}',
      templateUrl: '/posts.html',
      controller: 'PostsCtrl',
      resolve: {
        post: ['$stateParams', 'posts', function($stateParams, posts) {
          return posts.get($stateParams.id);
        }]
      }
    })
    .state('counter', {
      url: '/counter',
      templateUrl: '/counter.html',
      controller: 'CounterCtrl',
      resolve: {
        postPromise: ['posts', function (posts) {
          return posts.getCounters();
        }]
      }
    });

  $urlRouterProvider.otherwise('superinternetloveyou');

  //  $locationProvider.html5Mode(true);

}]);

app.controller('MainCtrl', [
'$scope', 'posts', '$interval', '$location',
function($scope, posts, $interval, $location){

if ($location.hash()=='superinternet') {
  $scope.admin = true;
} else {
  $scope.admin = false;
}

if ($location.hash()=='add') {
  $scope.addArtist = true;
} else {
  $scope.addArtist = false;
}

$scope.posts = posts.posts;
$scope.counter = posts.counters;

$scope.addPost = function(){
  var beginValue = new Date("June "+this.day+", 2016 "+this.hour+":"+this.minute+":00");
  // if(!$scope.name || $scope.name === '') { return; }
  posts.create({
    order: this.order,
    name: this.name,
    place: this.place,
    begin: beginValue.getTime(),
    status: this.status,
    delay: this.delay
  });
  $scope.name = '';
  $scope.place = '';
  $scope.order = '';

  $scope.posts = posts.posts;

};

// $scope.incrementUpvotes = function(post) {
//   posts.upvote(post);
// };


}]);

app.controller('PostsCtrl', [
'$scope',
'posts',
'post',
function($scope, posts, post){

  $scope.post = post;

  $scope.editPost = function(){

    // default values

    if ($scope.order) {
      orderValue = this.order;
    } else {
      orderValue = post.order;
    }

    if ($scope.name) {
      nameValue = this.name;
    } else {
      nameValue = post.name;
    }

    if ($scope.place) {
      placeValue = this.place;
    } else {
      placeValue = post.place;
    }

    if ($scope.day) {
      beginValueDate = new Date("June "+this.day+", 2016 "+this.hour+":"+this.minute+":00");
      beginValue = beginValueDate.getTime();
    } else {
      beginValue = post.begin;
    }

    if ($scope.status) {
      statusValue = this.status;
    } else {
      statusValue = post.status;
    }

    if ($scope.delay) {
      delayValue = this.delay;
    } else {
      delayValue = post.delay;
    }

    postsEdited = {
      _id: post._id,
      order: orderValue,
      name: nameValue,
      place: placeValue,
      begin: beginValue,
      status: statusValue,
      delay: delayValue
    };

    posts.update(postsEdited);
  };

}]);

app.controller('CounterCtrl', ['$scope', 'posts', '$interval', '$location', function ($scope, posts, $interval, $location) {
  $scope.counter = posts.counters;

  $scope.addVisitor = function() {
    posts.upvoteCounter(this.c);
  };

}]);

app.controller('FlightController',['$scope', function ($scope) {
  // <option value="Boarding">Boarding</option>
  // <option value="In Flight">In Flight</option>
  // <option value="Landed">Landed</option>
  // <option value="Delayed">Delayed</option>
  // <option value="Cancelled">Cancelled</option>

  currentStatus = ' ';
  currentStatusDelay = ' ';

  $scope.flight = $scope.$parent.flight;
  if ($scope.flight) {
    var delayTime = 0;
    if ($scope.flight.delay) {
      var delayTime = $scope.flight.delay*60*1000;
      currentStatusDelay = $scope.flight.delay + "'";
    }
    var initialTime = $scope.flight.begin + delayTime;
    var boardingValue = 10*60*1000;

    // console.log($scope.flight.order);
    // console.log(initialTime);
    if ($scope.flight.status != "Automatic" && $scope.flight.status != "") {
      currentStatus = $scope.flight.status;
    }
    if ($scope.flight.status == "Automatic" || $scope.flight.status == "" || $scope.flight.status == "Delayed") {
      var currentTimeDate = new Date();
      var currentTime = currentTimeDate.getTime();
      // console.log("initialTime: "+initialTime);
      // console.log("boardingValue: "+boardingValue);
      var boardingTime = initialTime - boardingValue;
      // console.log("currentTime: "+currentTime);
      console.log("boardingTime: "+boardingTime);
      // console.log("initialTime: "+initialTime);
      if (currentTime > boardingTime && currentTime < initialTime) {
        currentStatus = "Boarding";
      } else if (currentTime >= initialTime) {
        currentStatus = "In Flight";
        currentStatusDelay = " ";
      }
    }
    if ($scope.flight.status == "Cancelled" || currentStatus  == "Landed") {
      currentStatusDelay = " ";
    }

  }

  $scope.statusText = currentStatus;
  $scope.statusDelay = currentStatusDelay;

}])

app.filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
});

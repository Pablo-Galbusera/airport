var express = require('express');

var mongoose = require('mongoose');

var Flight = mongoose.model('Flight');
var Counter = mongoose.model('Counter');

// Flight.collection.remove();

var router = express.Router();

router.get('/posts', function(req, res, next) {
  Flight.find(function(err, posts){
    if(err){ return next(err); }

    res.json(posts);
  });
});

router.post('/posts', function(req, res, next) {
  var post = new Flight(req.body);

  post.save(function(err, post){
    if(err){ return next(err); }

    res.json(post);
  });
});

router.param('post', function(req, res, next, id) {
  var query = Flight.findById(id);

  query.exec(function (err, post){
    if (err) { return next(err); }
    if (!post) { return next(new Error('can\'t find post')); }

    req.post = post;
    return next();
  });
});

router.get('/posts/:post', function(req, res, next) {
  req.post.populate('comments', function(err, post) {
    if (err) { return next(err); }

    res.json(post);
  });
});

router.put('/posts/:post/upvote', function(req, res, next) {
  req.post.upvote(function(err, post){
    if (err) { return next(err); }

    res.json(post);
  });
});

router.post('/posts/:post/update',function (req, res, next) {

  // console.log(req.body);

  Flight.findById(req.body._id, function(err, f) {
    if (!f) {
      console.log('Error');
    } else {
      f.update(req.body, function (err, raw) {
        // console.log('The raw response from Mongo was ', raw);
        if (err) {
            return handleError(err);
        } else {
          res.json({test: 'ok'});
        }
      });
    }
  });

});

router.get('/counter', function (req, res, next) {
  Counter.find(function(err, counters){
    if(err){ return next(err); }
    res.json(counters);
  });
});

router.post('/counter', function(req, res, next) {
  var counter = new Counter(req.body);

  counter.save(function(err, counter){
    if(err){ return next(err); }
    // console.log(counter);
    res.json(counter);
  });

});

router.param('counter', function(req, res, next, id) {
  var query = Counter.findById(id);

  query.exec(function (err, counter){
    if (err) { return next(err); }
    if (!counter) { return next(new Error('can\'t find counter')); }

    req.counter = counter;
    return next();
  });
});

router.put('/counters/:counter/upvote', function(req, res, next) {
  req.counter.upvote(function(err, counter){
    if (err) { return next(err); }
    console.log('update vote');
    res.json(counter);
  });
});


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
